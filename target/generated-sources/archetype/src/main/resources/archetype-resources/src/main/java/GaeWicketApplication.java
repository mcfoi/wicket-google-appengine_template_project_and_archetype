#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};


import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;

public class GaeWicketApplication extends WebApplication
{
    @Override
    public Class<? extends WebPage> getHomePage() {
      return HomePage.class;
    }

    @Override
    protected void init() {
	  
	  getRequestCycleSettings().setResponseRequestEncoding("UTF-8"); 
      getMarkupSettings().setDefaultMarkupEncoding("UTF-8"); 
	  	
      super.init();
      getResourceSettings().setResourcePollFrequency(null);
    }

}
